const data = {
    store: []
};
const Store = {

    add: (item) => data.store.push(item),
    get: () => [ ...data.store ]
}

export default Store;
import Order from './Order';
let orderList = document.querySelector(".order-list");

export const order = new Order(); 

class Place {
    constructor(row, place, status = 'free') {
      this.row = row;
      this.place = place;
      this.status = status;
      this.price = 120;
  
      this.click = this.click.bind(this);
    }
  
    click(e) {
      e.target.className = e.target.className + " test";
      order.add(this);
      order.render(orderList);
      console.log(this);

      setTimeout(() => {
        e.target.className = "place";
        order.getOrders().forEach((item, index) => {
            if (item.row === this.row && item.place === this.place) {
                order.delete(index);
                order.render(orderList);
            } 
        })
      }, 10000);
    }
  
    render(node) {
      let item = document.createElement("div");
      item.textContent = this.status === "free" ? this.place : "X";
      item.onclick = this.status === "free" ? this.click : null;
      item.className = this.status === "free" ? "place" : "isBought";
      node.appendChild(item);
    }
  }

  export default Place;
  
import Place from "./Place";
import { order } from "./Place";

let main = document.querySelector(".places");
let addButton = document.querySelector("#add");
let orderList = document.querySelector(".order-list");

let cinema = [];

addButton.addEventListener("click", e => {
  let orders = order.getOrders();
  orders.forEach(order => {
    cinema.forEach((item, index) => {
      if (order.place === item.place && order.row === item.row) {
        cinema[index].status = "isBought";
      }
    });
  });
  localStorage.setItem("places", JSON.stringify(cinema));
  order.clear();
  order.render(orderList)
  renderCinema();
});

renderCinema();
// let order = new Order();

function renderCinema() {
  if (localStorage.hasOwnProperty("places")) {
    let places = JSON.parse(localStorage["places"]);
    if (places.length) {
        main.innerHTML = '';
        cinema = [];
        console.log(Math.max())
        places.map( item => {
          cinema.push(new Place(item.row, item.place, item.status));
          cinema[cinema.length - 1].render(main);
          if (item.place == 15)
            main.appendChild(document.createElement("br"));
        }); 
    } else {
      renderDefault();
    }
  } else {
    renderDefault();
  }
}

function renderDefault() {
    main.innerHTML = '';
    for (let row = 1; row <= 10; row++) {
        for (let place = 1; place <= 15; place++) {
            cinema.push(new Place(row, place));
            cinema[cinema.length - 1].render(main);
        }
        main.appendChild(document.createElement("br"));
      }   
}

class Order {
  constructor() {
    this.orders = [];
    // this.place = place;
  }

  getOrders() {
    return this.orders;
  }
  delete(index) {
    this.orders.splice(index, 1);
  }

  clear() {
    this.orders = [];
  }

  add(place) {
    this.orders.push(place);
  }
  render(node) {
    node.innerHTML = "";
    this.orders.map(place => {
      let item = document.createElement("div");
      item.innerHTML = `
          <div>
              ${place.row} ряд, ${place.place} место, цена ${place.price};
          </div>
      `;
      node.appendChild(item);
    });
    // let { place } = this;
  }
}

export default Order;

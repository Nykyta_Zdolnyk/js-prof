/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/Order.js":
/*!******************************!*\
  !*** ./application/Order.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nclass Order {\r\n  constructor() {\r\n    this.orders = [];\r\n    // this.place = place;\r\n  }\r\n\r\n  getOrders() {\r\n    return this.orders;\r\n  }\r\n  delete(index) {\r\n    this.orders.splice(index, 1);\r\n  }\r\n\r\n  clear() {\r\n    this.orders = [];\r\n  }\r\n\r\n  add(place) {\r\n    this.orders.push(place);\r\n  }\r\n  render(node) {\r\n    node.innerHTML = \"\";\r\n    this.orders.map(place => {\r\n      let item = document.createElement(\"div\");\r\n      item.innerHTML = `\r\n          <div>\r\n              ${place.row} ряд, ${place.place} место, цена ${place.price};\r\n          </div>\r\n      `;\r\n      node.appendChild(item);\r\n    });\r\n    // let { place } = this;\r\n  }\r\n}\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Order);\r\n\n\n//# sourceURL=webpack:///./application/Order.js?");

/***/ }),

/***/ "./application/Place.js":
/*!******************************!*\
  !*** ./application/Place.js ***!
  \******************************/
/*! exports provided: order, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"order\", function() { return order; });\n/* harmony import */ var _Order__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Order */ \"./application/Order.js\");\n\r\nlet orderList = document.querySelector(\".order-list\");\r\n\r\nconst order = new _Order__WEBPACK_IMPORTED_MODULE_0__[\"default\"](); \r\n\r\nclass Place {\r\n    constructor(row, place, status = 'free') {\r\n      this.row = row;\r\n      this.place = place;\r\n      this.status = status;\r\n      this.price = 120;\r\n  \r\n      this.click = this.click.bind(this);\r\n    }\r\n  \r\n    click(e) {\r\n      e.target.className = e.target.className + \" test\";\r\n      order.add(this);\r\n      order.render(orderList);\r\n      console.log(this);\r\n\r\n      setTimeout(() => {\r\n        e.target.className = \"place\";\r\n        order.getOrders().forEach((item, index) => {\r\n            if (item.row === this.row && item.place === this.place) {\r\n                order.delete(index);\r\n                order.render(orderList);\r\n            } \r\n        })\r\n      }, 10000);\r\n    }\r\n  \r\n    render(node) {\r\n      let item = document.createElement(\"div\");\r\n      item.textContent = this.status === \"free\" ? this.place : \"X\";\r\n      item.onclick = this.status === \"free\" ? this.click : null;\r\n      item.className = this.status === \"free\" ? \"place\" : \"isBought\";\r\n      node.appendChild(item);\r\n    }\r\n  }\r\n\r\n  /* harmony default export */ __webpack_exports__[\"default\"] = (Place);\r\n  \n\n//# sourceURL=webpack:///./application/Place.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Place__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Place */ \"./application/Place.js\");\n\r\n\r\n\r\nlet main = document.querySelector(\".places\");\r\nlet addButton = document.querySelector(\"#add\");\r\nlet orderList = document.querySelector(\".order-list\");\r\n\r\nlet cinema = [];\r\n\r\naddButton.addEventListener(\"click\", e => {\r\n  let orders = _Place__WEBPACK_IMPORTED_MODULE_0__[\"order\"].getOrders();\r\n  orders.forEach(order => {\r\n    cinema.forEach((item, index) => {\r\n      if (order.place === item.place && order.row === item.row) {\r\n        cinema[index].status = \"isBought\";\r\n      }\r\n    });\r\n  });\r\n  localStorage.setItem(\"places\", JSON.stringify(cinema));\r\n  _Place__WEBPACK_IMPORTED_MODULE_0__[\"order\"].clear();\r\n  _Place__WEBPACK_IMPORTED_MODULE_0__[\"order\"].render(orderList)\r\n  renderCinema();\r\n});\r\n\r\nrenderCinema();\r\n// let order = new Order();\r\n\r\nfunction renderCinema() {\r\n  if (localStorage.hasOwnProperty(\"places\")) {\r\n    let places = JSON.parse(localStorage[\"places\"]);\r\n    if (places.length) {\r\n        main.innerHTML = '';\r\n        cinema = [];\r\n        console.log(Math.max())\r\n        places.map( item => {\r\n          cinema.push(new _Place__WEBPACK_IMPORTED_MODULE_0__[\"default\"](item.row, item.place, item.status));\r\n          cinema[cinema.length - 1].render(main);\r\n          if (item.place == 15)\r\n            main.appendChild(document.createElement(\"br\"));\r\n        }); \r\n    } else {\r\n      renderDefault();\r\n    }\r\n  } else {\r\n    renderDefault();\r\n  }\r\n}\r\n\r\nfunction renderDefault() {\r\n    main.innerHTML = '';\r\n    for (let row = 1; row <= 10; row++) {\r\n        for (let place = 1; place <= 15; place++) {\r\n            cinema.push(new _Place__WEBPACK_IMPORTED_MODULE_0__[\"default\"](row, place));\r\n            cinema[cinema.length - 1].render(main);\r\n        }\r\n        main.appendChild(document.createElement(\"br\"));\r\n      }   \r\n}\r\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ })

/******/ });
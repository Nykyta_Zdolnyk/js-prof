/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _singleton_singleton__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./singleton/singleton */ \"./application/singleton/singleton.js\");\n\r\n\r\n\r\n  console.log(_singleton_singleton__WEBPACK_IMPORTED_MODULE_0__[\"default\"].showAllLaws());\r\n  console.log(_singleton_singleton__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getLaw(0))\r\n  console.log(_singleton_singleton__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getCitizensSatisfactions());\r\n  console.log(_singleton_singleton__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getBudget());\r\n  console.log(_singleton_singleton__WEBPACK_IMPORTED_MODULE_0__[\"default\"].spendHoliday());\r\n  _singleton_singleton__WEBPACK_IMPORTED_MODULE_0__[\"default\"].add({id:1, name: 'law2', description: \"law2 text\"})\r\n  console.log(_singleton_singleton__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getBudget());\r\n  console.log(_singleton_singleton__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getCitizensSatisfactions());\r\n  console.log(_singleton_singleton__WEBPACK_IMPORTED_MODULE_0__[\"default\"].showAllLaws());\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/singleton/singleton.js":
/*!********************************************!*\
  !*** ./application/singleton/singleton.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\r\n  Задание:\r\n\r\n    Написать синглтон, который будет создавать обьект government\r\n\r\n    Данные:\r\n    {\r\n        laws: [\r\n        {\r\n          id: 0,\r\n          text: '123123'\r\n        }\r\n      ],\r\n        budget: 1000000\r\n        citizensSatisfactions: 0,\r\n    }\r\n\r\n    У этого обьекта будут методы:\r\n      .добавитьЗакон({id, name, description})\r\n        -> добавляет закон в laws и понижает удовлетворенность граждан на -10\r\n\r\n      .читатькКонституцию -> Вывести все законы на экран\r\n      .читатьЗакон(ид)\r\n\r\n      .показатьУровеньДовольства()\r\n      .показатьБюджет()\r\n      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5\r\n\r\n\r\n*/\r\n\r\nconst data = {\r\n    laws: [\r\n    {\r\n      id: 0,\r\n      text: '123123'\r\n    }\r\n  ],\r\n    budget: 1000000,\r\n    citizensSatisfactions: 0,\r\n  };\r\n  \r\n  const government = {\r\n    add: (item) => {\r\n      data.laws.push(item);\r\n      data.citizensSatisfactions -= 10;\r\n    },\r\n    showAllLaws: () =>  [ ...data.laws ] ,\r\n    getLaw: (id) =>  data.laws.find( law => law.id === id ) ,\r\n    getCitizensSatisfactions: () =>  data.citizensSatisfactions ,\r\n    getBudget: () =>  data.budget ,\r\n    spendHoliday: () => {\r\n      data.budget -= 50000;\r\n      data.citizensSatisfactions += 5;\r\n    }\r\n  }\r\n  \r\n  Object.freeze(government);\r\n\r\n  \r\n  /* harmony default export */ __webpack_exports__[\"default\"] = (government);\r\n  \n\n//# sourceURL=webpack:///./application/singleton/singleton.js?");

/***/ })

/******/ });
/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [
        {
          id: 0,
          text: '123123'
        }
      ],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/

const data = {
    laws: [
    {
      id: 0,
      text: '123123'
    }
  ],
    budget: 1000000,
    citizensSatisfactions: 0,
  };
  
  const government = {
    add: (item) => {
      data.laws.push(item);
      data.citizensSatisfactions -= 10;
    },
    showAllLaws: () =>  [ ...data.laws ] ,
    getLaw: (id) =>  data.laws.find( law => law.id === id ) ,
    getCitizensSatisfactions: () =>  data.citizensSatisfactions ,
    getBudget: () =>  data.budget ,
    spendHoliday: () => {
      data.budget -= 50000;
      data.citizensSatisfactions += 5;
    }
  }
  
  Object.freeze(government);

  
  export default government;
  
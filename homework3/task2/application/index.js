import work from './objectfreeze/objectfreeze';

let universe = {
    infinity: Infinity,
    good: ["cats", "love", "rock-n-roll"],
    evil: {
      bonuses: ["cookies", "great look"]
    }
  };

let FarGalaxy = work(universe);
    FarGalaxy.good.push('javascript'); // false
    FarGalaxy.something = 'Wow!'; // false
    FarGalaxy.evil.humans = [];   // false

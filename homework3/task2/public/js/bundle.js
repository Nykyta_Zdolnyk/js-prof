/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _objectfreeze_objectfreeze__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./objectfreeze/objectfreeze */ \"./application/objectfreeze/objectfreeze.js\");\n\r\n\r\nlet universe = {\r\n    infinity: Infinity,\r\n    good: [\"cats\", \"love\", \"rock-n-roll\"],\r\n    evil: {\r\n      bonuses: [\"cookies\", \"great look\"]\r\n    }\r\n  };\r\n\r\nlet FarGalaxy = Object(_objectfreeze_objectfreeze__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(universe);\r\n    FarGalaxy.good.push('javascript'); // false\r\n    FarGalaxy.something = 'Wow!'; // false\r\n    FarGalaxy.evil.humans = [];   // false\r\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/objectfreeze/objectfreeze.js":
/*!**************************************************!*\
  !*** ./application/objectfreeze/objectfreeze.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\r\n  Задание: написать функцию, для глубокой заморозки обьектов.\r\n\r\n  Обьект для работы:\r\n  let universe = {\r\n    infinity: Infinity,\r\n    good: ['cats', 'love', 'rock-n-roll'],\r\n    evil: {\r\n      bonuses: ['cookies', 'great look']\r\n    }\r\n  };\r\n\r\n  frozenUniverse.evil.humans = []; -> Не должен отработать.\r\n\r\n  Методы для работы:\r\n  1. Object.getOwnPropertyNames(obj);\r\n      -> Получаем имена свойств из объекта obj в виде массива\r\n\r\n  2. Проверка через typeof на обьект или !== null\r\n  if (typeof prop == 'object' && prop !== null){...}\r\n\r\n  Тестирование:\r\n\r\n  let FarGalaxy = DeepFreeze(universe);\r\n      FarGalaxy.good.push('javascript'); // false\r\n      FarGalaxy.something = 'Wow!'; // false\r\n      FarGalaxy.evil.humans = [];   // false\r\n\r\n*/\r\n\r\nconst work = obj => {\r\n  console.log(\"your code \");\r\n  let keys = Object.getOwnPropertyNames(obj);\r\n  // console.log(keys);\r\n\r\n  keys.forEach(key => {\r\n    if (typeof obj[key] == \"object\" && obj[key] !== null) {\r\n      work(obj[key]);\r\n    }\r\n  });\r\n\r\n  return Object.freeze(obj);\r\n};\r\n\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (work);\r\n\n\n//# sourceURL=webpack:///./application/objectfreeze/objectfreeze.js?");

/***/ })

/******/ });
const workers = [
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa0777625a8f42a195283", 
      "name": "Toni Monroe", 
      "gender": "female", 
      "age": 30, 
      "phone": "+1 (806) 580-2329", 
      "rate": 13.9563, 
      "balance": "$2,572.35", 
      "type": "backend", 
      "email": "tonimonroe@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa077693a3e746b118148", 
      "name": "Lynnette Kinney", 
      "gender": "female", 
      "age": 28, 
      "phone": "+1 (870) 472-3682", 
      "rate": 13.01, 
      "balance": "$3,336.80", 
      "type": "frontend", 
      "email": "lynnettekinney@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa077577a0177cefe1bb4", 
      "name": "David Madden", 
      "gender": "male", 
      "age": 26, 
      "phone": "+1 (872) 429-3310", 
      "rate": 3.9864, 
      "balance": "$2,905.09", 
      "type": "project", 
      "email": "davidmadden@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa0773482e6e2ddeaf56b", 
      "name": "Francis Lott", 
      "gender": "female", 
      "age": 21, 
      "phone": "+1 (857) 557-3568", 
      "rate": 16.3074, 
      "balance": "$2,210.12", 
      "type": "project", 
      "email": "francislott@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa077fc484637f25618f5", 
      "name": "Hilary Melendez", 
      "gender": "female", 
      "age": 24, 
      "phone": "+1 (833) 407-2182", 
      "rate": 4.661, 
      "balance": "$1,503.64", 
      "type": "project", 
      "email": "hilarymelendez@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa07755c12047f30a3484", 
      "name": "Hewitt Hull", 
      "gender": "male", 
      "age": 30, 
      "phone": "+1 (895) 432-2420", 
      "rate": 19.5554, 
      "balance": "$1,524.77", 
      "type": "project", 
      "email": "hewitthull@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa0777e1e6338122ef3fb", 
      "name": "Kelley Tate", 
      "gender": "male", 
      "age": 30, 
      "phone": "+1 (878) 533-2028", 
      "rate": 9.6464, 
      "balance": "$3,831.38", 
      "type": "backend", 
      "email": "kelleytate@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa07721ca780f7e5b448f", 
      "name": "Gillespie Sargent", 
      "gender": "male", 
      "age": 30, 
      "phone": "+1 (849) 509-3525", 
      "rate": 2.2923, 
      "balance": "$1,466.05", 
      "type": "project", 
      "email": "gillespiesargent@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa0770ae202435ac9dac0", 
      "name": "Torres Dennis", 
      "gender": "male", 
      "age": 39, 
      "phone": "+1 (803) 448-2081", 
      "rate": 2.0159, 
      "balance": "$1,895.85", 
      "type": "frontend", 
      "email": "torresdennis@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa07792c57a85d5c93f6d", 
      "name": "Fitzgerald Tran", 
      "gender": "male", 
      "age": 39, 
      "phone": "+1 (820) 577-3539", 
      "rate": 4.0788, 
      "balance": "$1,909.15", 
      "type": "backend", 
      "email": "fitzgeraldtran@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa077b37e59096a60759f", 
      "name": "Trisha Navarro", 
      "gender": "female", 
      "age": 25, 
      "phone": "+1 (912) 567-3361", 
      "rate": 2.3353, 
      "balance": "$2,698.21", 
      "type": "backend", 
      "email": "trishanavarro@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa0775591945c9b303e0b", 
      "name": "Yesenia Sanford", 
      "gender": "female", 
      "age": 24, 
      "phone": "+1 (915) 515-2404", 
      "rate": 5.1389, 
      "balance": "$2,000.88", 
      "type": "backend", 
      "email": "yeseniasanford@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa07785b2ad8580414ca2", 
      "name": "Jean Greer", 
      "gender": "female", 
      "age": 20, 
      "phone": "+1 (870) 449-3526", 
      "rate": 6.0656, 
      "balance": "$1,199.76", 
      "type": "design", 
      "email": "jeangreer@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa077c565efeaa7b46094", 
      "name": "Savannah Leonard", 
      "gender": "female", 
      "age": 28, 
      "phone": "+1 (978) 585-3839", 
      "rate": 16.2476, 
      "balance": "$3,224.64", 
      "type": "design", 
      "email": "savannahleonard@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa077f5117eabe790abca", 
      "name": "Megan Johns", 
      "gender": "female", 
      "age": 21, 
      "phone": "+1 (937) 474-3893", 
      "rate": 18.5682, 
      "balance": "$2,744.53", 
      "type": "design", 
      "email": "meganjohns@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa077f3ebd22d817474b9", 
      "name": "Chen Mays", 
      "gender": "male", 
      "age": 23, 
      "phone": "+1 (918) 406-2279", 
      "rate": 7.1135, 
      "balance": "$3,451.56", 
      "type": "frontend", 
      "email": "chenmays@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa07733f4129e08d51844", 
      "name": "Sheree Santiago", 
      "gender": "female", 
      "age": 23, 
      "phone": "+1 (979) 531-3464", 
      "rate": 13.3253, 
      "balance": "$1,725.93", 
      "type": "design", 
      "email": "shereesantiago@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa077f6894a1c486fd2da", 
      "name": "Rosemarie Hays", 
      "gender": "female", 
      "age": 28, 
      "phone": "+1 (868) 432-2831", 
      "rate": 3.7168, 
      "balance": "$3,854.52", 
      "type": "project", 
      "email": "rosemariehays@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa07732c4cd5ea248835d", 
      "name": "Josie Rodriquez", 
      "gender": "female", 
      "age": 22, 
      "phone": "+1 (974) 513-3053", 
      "rate": 18.0521, 
      "balance": "$1,408.24", 
      "type": "project", 
      "email": "josierodriquez@sureplex.com"
    }, 
    {
      "picture": "http://placehold.it/32x32", 
      "_id": "5b9aa0771f1b627374614ca6", 
      "name": "Hernandez Shepherd", 
      "gender": "male", 
      "age": 28, 
      "phone": "+1 (862) 558-2379", 
      "rate": 8.7845, 
      "balance": "$2,915.25", 
      "type": "design", 
      "email": "hernandezshepherd@sureplex.com"
    }
  ]

  export default workers;
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _workers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./workers */ \"./application/workers.js\");\n/*\r\n\r\n  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет\r\n  расспределять и создавать сотрудников компании нужного типа.\r\n\r\n  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2\r\n\r\n  HeadHunt => {\r\n    hire( obj ){\r\n      ...\r\n    }\r\n  }\r\n\r\n  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики\r\n\r\n*/\r\n\r\n\r\nlet test = _workers__WEBPACK_IMPORTED_MODULE_0__[\"default\"].map(item => {\r\n    let { name, gender, age } = item;\r\n    return `\r\n        <div>\r\n            ${name} (${age}) \r\n        </div>\r\n    `\r\n});\r\n\r\ndocument.body.innerHTML = test;\r\n\r\n\r\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/workers.js":
/*!********************************!*\
  !*** ./application/workers.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst workers = [\r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa0777625a8f42a195283\", \r\n      \"name\": \"Toni Monroe\", \r\n      \"gender\": \"female\", \r\n      \"age\": 30, \r\n      \"phone\": \"+1 (806) 580-2329\", \r\n      \"rate\": 13.9563, \r\n      \"balance\": \"$2,572.35\", \r\n      \"type\": \"backend\", \r\n      \"email\": \"tonimonroe@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa077693a3e746b118148\", \r\n      \"name\": \"Lynnette Kinney\", \r\n      \"gender\": \"female\", \r\n      \"age\": 28, \r\n      \"phone\": \"+1 (870) 472-3682\", \r\n      \"rate\": 13.01, \r\n      \"balance\": \"$3,336.80\", \r\n      \"type\": \"frontend\", \r\n      \"email\": \"lynnettekinney@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa077577a0177cefe1bb4\", \r\n      \"name\": \"David Madden\", \r\n      \"gender\": \"male\", \r\n      \"age\": 26, \r\n      \"phone\": \"+1 (872) 429-3310\", \r\n      \"rate\": 3.9864, \r\n      \"balance\": \"$2,905.09\", \r\n      \"type\": \"project\", \r\n      \"email\": \"davidmadden@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa0773482e6e2ddeaf56b\", \r\n      \"name\": \"Francis Lott\", \r\n      \"gender\": \"female\", \r\n      \"age\": 21, \r\n      \"phone\": \"+1 (857) 557-3568\", \r\n      \"rate\": 16.3074, \r\n      \"balance\": \"$2,210.12\", \r\n      \"type\": \"project\", \r\n      \"email\": \"francislott@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa077fc484637f25618f5\", \r\n      \"name\": \"Hilary Melendez\", \r\n      \"gender\": \"female\", \r\n      \"age\": 24, \r\n      \"phone\": \"+1 (833) 407-2182\", \r\n      \"rate\": 4.661, \r\n      \"balance\": \"$1,503.64\", \r\n      \"type\": \"project\", \r\n      \"email\": \"hilarymelendez@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa07755c12047f30a3484\", \r\n      \"name\": \"Hewitt Hull\", \r\n      \"gender\": \"male\", \r\n      \"age\": 30, \r\n      \"phone\": \"+1 (895) 432-2420\", \r\n      \"rate\": 19.5554, \r\n      \"balance\": \"$1,524.77\", \r\n      \"type\": \"project\", \r\n      \"email\": \"hewitthull@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa0777e1e6338122ef3fb\", \r\n      \"name\": \"Kelley Tate\", \r\n      \"gender\": \"male\", \r\n      \"age\": 30, \r\n      \"phone\": \"+1 (878) 533-2028\", \r\n      \"rate\": 9.6464, \r\n      \"balance\": \"$3,831.38\", \r\n      \"type\": \"backend\", \r\n      \"email\": \"kelleytate@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa07721ca780f7e5b448f\", \r\n      \"name\": \"Gillespie Sargent\", \r\n      \"gender\": \"male\", \r\n      \"age\": 30, \r\n      \"phone\": \"+1 (849) 509-3525\", \r\n      \"rate\": 2.2923, \r\n      \"balance\": \"$1,466.05\", \r\n      \"type\": \"project\", \r\n      \"email\": \"gillespiesargent@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa0770ae202435ac9dac0\", \r\n      \"name\": \"Torres Dennis\", \r\n      \"gender\": \"male\", \r\n      \"age\": 39, \r\n      \"phone\": \"+1 (803) 448-2081\", \r\n      \"rate\": 2.0159, \r\n      \"balance\": \"$1,895.85\", \r\n      \"type\": \"frontend\", \r\n      \"email\": \"torresdennis@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa07792c57a85d5c93f6d\", \r\n      \"name\": \"Fitzgerald Tran\", \r\n      \"gender\": \"male\", \r\n      \"age\": 39, \r\n      \"phone\": \"+1 (820) 577-3539\", \r\n      \"rate\": 4.0788, \r\n      \"balance\": \"$1,909.15\", \r\n      \"type\": \"backend\", \r\n      \"email\": \"fitzgeraldtran@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa077b37e59096a60759f\", \r\n      \"name\": \"Trisha Navarro\", \r\n      \"gender\": \"female\", \r\n      \"age\": 25, \r\n      \"phone\": \"+1 (912) 567-3361\", \r\n      \"rate\": 2.3353, \r\n      \"balance\": \"$2,698.21\", \r\n      \"type\": \"backend\", \r\n      \"email\": \"trishanavarro@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa0775591945c9b303e0b\", \r\n      \"name\": \"Yesenia Sanford\", \r\n      \"gender\": \"female\", \r\n      \"age\": 24, \r\n      \"phone\": \"+1 (915) 515-2404\", \r\n      \"rate\": 5.1389, \r\n      \"balance\": \"$2,000.88\", \r\n      \"type\": \"backend\", \r\n      \"email\": \"yeseniasanford@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa07785b2ad8580414ca2\", \r\n      \"name\": \"Jean Greer\", \r\n      \"gender\": \"female\", \r\n      \"age\": 20, \r\n      \"phone\": \"+1 (870) 449-3526\", \r\n      \"rate\": 6.0656, \r\n      \"balance\": \"$1,199.76\", \r\n      \"type\": \"design\", \r\n      \"email\": \"jeangreer@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa077c565efeaa7b46094\", \r\n      \"name\": \"Savannah Leonard\", \r\n      \"gender\": \"female\", \r\n      \"age\": 28, \r\n      \"phone\": \"+1 (978) 585-3839\", \r\n      \"rate\": 16.2476, \r\n      \"balance\": \"$3,224.64\", \r\n      \"type\": \"design\", \r\n      \"email\": \"savannahleonard@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa077f5117eabe790abca\", \r\n      \"name\": \"Megan Johns\", \r\n      \"gender\": \"female\", \r\n      \"age\": 21, \r\n      \"phone\": \"+1 (937) 474-3893\", \r\n      \"rate\": 18.5682, \r\n      \"balance\": \"$2,744.53\", \r\n      \"type\": \"design\", \r\n      \"email\": \"meganjohns@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa077f3ebd22d817474b9\", \r\n      \"name\": \"Chen Mays\", \r\n      \"gender\": \"male\", \r\n      \"age\": 23, \r\n      \"phone\": \"+1 (918) 406-2279\", \r\n      \"rate\": 7.1135, \r\n      \"balance\": \"$3,451.56\", \r\n      \"type\": \"frontend\", \r\n      \"email\": \"chenmays@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa07733f4129e08d51844\", \r\n      \"name\": \"Sheree Santiago\", \r\n      \"gender\": \"female\", \r\n      \"age\": 23, \r\n      \"phone\": \"+1 (979) 531-3464\", \r\n      \"rate\": 13.3253, \r\n      \"balance\": \"$1,725.93\", \r\n      \"type\": \"design\", \r\n      \"email\": \"shereesantiago@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa077f6894a1c486fd2da\", \r\n      \"name\": \"Rosemarie Hays\", \r\n      \"gender\": \"female\", \r\n      \"age\": 28, \r\n      \"phone\": \"+1 (868) 432-2831\", \r\n      \"rate\": 3.7168, \r\n      \"balance\": \"$3,854.52\", \r\n      \"type\": \"project\", \r\n      \"email\": \"rosemariehays@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa07732c4cd5ea248835d\", \r\n      \"name\": \"Josie Rodriquez\", \r\n      \"gender\": \"female\", \r\n      \"age\": 22, \r\n      \"phone\": \"+1 (974) 513-3053\", \r\n      \"rate\": 18.0521, \r\n      \"balance\": \"$1,408.24\", \r\n      \"type\": \"project\", \r\n      \"email\": \"josierodriquez@sureplex.com\"\r\n    }, \r\n    {\r\n      \"picture\": \"http://placehold.it/32x32\", \r\n      \"_id\": \"5b9aa0771f1b627374614ca6\", \r\n      \"name\": \"Hernandez Shepherd\", \r\n      \"gender\": \"male\", \r\n      \"age\": 28, \r\n      \"phone\": \"+1 (862) 558-2379\", \r\n      \"rate\": 8.7845, \r\n      \"balance\": \"$2,915.25\", \r\n      \"type\": \"design\", \r\n      \"email\": \"hernandezshepherd@sureplex.com\"\r\n    }\r\n  ]\r\n\r\n  /* harmony default export */ __webpack_exports__[\"default\"] = (workers);\n\n//# sourceURL=webpack:///./application/workers.js?");

/***/ })

/******/ });
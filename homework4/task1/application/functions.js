/*
  Composition:

  Задание при помощи композиции создать объекты 4х типов:

  functions:
    - MakeBackendMagic
    - MakeFrontendMagic
    - MakeItLooksBeautiful
    - DistributeTasks
    - DrinkSomeTea
    - WatchYoutube
    - Procrastinate

  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate
  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube
  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate
  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea

  ProjectManager(name,gender,age) => { name, gender, age, type: 'project'}

*/
const Composition = () => {

  const MakeBackendMagic = state => ({
    make: () => console.log(`${state.name} make Backend Magic`)
  });

  const MakeFrontendMagic = state => ({
    make: () => console.log(`${state.name} make Frontend Magic`)
  });

  const MakeItLooksBeautiful = state => ({
    make: () => console.log(`${state.name} make it look beautiful`)
  });

  const DistributeTasks = state => ({
    distribute: () => console.log(`${state.name} distributes tasks`)
  });

  const DrinkSomeTea = state => ({
    drink: () => console.log(`${state.name} drinks some tea`)
  });

  const WatchYoutube = state => ({
    watch: () => console.log(`${state.name} watches youtube`)
  });

  const Procrastinate = state => ({
    procrastinate: () => console.log(`${state.name} procrastinates`)
  });

  //BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate
  const BackendDeveloper = (name, gender, age) => {
    let state = {
      name,
      gender,
      age,
      type: 'backend'

    };

    return Object.assign(
      {},
      state,
      MakeBackendMagic(state),
      DrinkSomeTea(state),
      Procrastinate(state)
    );
  };

  // FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube
  const FrontendDeveloper = (name, gender, age) => {
    let state = {
      name,
      gender,
      age,
      type: 'front'
    };

    return Object.assign(
      {},
      state,
      MakeFrontendMagic(state),
      DrinkSomeTea(state),
      WatchYoutube(state)
    );
  };

  // Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate
  const Designer = (name, gender, age) => {
    let state = {
      name,
      gender,
      age,
      type: 'design'
    };

    return Object.assign(
      {},
      state,
      MakeItLooksBeautiful(state),
      WatchYoutube(state),
      Procrastinate(state)
    );
  };

  //ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea
  const ProjectManager = (name, gender, age) => {
    let state = {
      name,
      gender,
      age,
      type: 'project'
    };

    return Object.assign(
      {},
      state,
      DistributeTasks(state),
      Procrastinate(state),
      DrinkSomeTea(state)
    );
  };

  const Nick = BackendDeveloper("Nick", 'male', 25);
        Nick.make();
        Nick.drink();
        Nick.procrastinate();
        console.log("NICK", Nick)

  const Fred = FrontendDeveloper("Fred", 'male', 33);
        Fred.make();
        Fred.drink();
        Fred.watch();

  const Anna = Designer("Anna", 'female', 19);
        Anna.make();
        Anna.watch();
        Anna.procrastinate();

  const Max = ProjectManager("Max", 'male', 22);
        Max.distribute();
        Max.procrastinate();
        Max.drink();
};
export default Composition;

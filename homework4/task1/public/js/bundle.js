/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/functions.js":
/*!**********************************!*\
  !*** ./application/functions.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\r\n  Composition:\r\n\r\n  Задание при помощи композиции создать объекты 4х типов:\r\n\r\n  functions:\r\n    - MakeBackendMagic\r\n    - MakeFrontendMagic\r\n    - MakeItLooksBeautiful\r\n    - DistributeTasks\r\n    - DrinkSomeTea\r\n    - WatchYoutube\r\n    - Procrastinate\r\n\r\n  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate\r\n  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube\r\n  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate\r\n  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea\r\n\r\n  ProjectManager(name,gender,age) => { name, gender, age, type: 'project'}\r\n\r\n*/\r\nconst Composition = () => {\r\n\r\n  const MakeBackendMagic = state => ({\r\n    make: () => console.log(`${state.name} make Backend Magic`)\r\n  });\r\n\r\n  const MakeFrontendMagic = state => ({\r\n    make: () => console.log(`${state.name} make Frontend Magic`)\r\n  });\r\n\r\n  const MakeItLooksBeautiful = state => ({\r\n    make: () => console.log(`${state.name} make it look beautiful`)\r\n  });\r\n\r\n  const DistributeTasks = state => ({\r\n    distribute: () => console.log(`${state.name} distributes tasks`)\r\n  });\r\n\r\n  const DrinkSomeTea = state => ({\r\n    drink: () => console.log(`${state.name} drinks some tea`)\r\n  });\r\n\r\n  const WatchYoutube = state => ({\r\n    watch: () => console.log(`${state.name} watches youtube`)\r\n  });\r\n\r\n  const Procrastinate = state => ({\r\n    procrastinate: () => console.log(`${state.name} procrastinates`)\r\n  });\r\n\r\n  //BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate\r\n  const BackendDeveloper = (name, gender, age) => {\r\n    let state = {\r\n      name,\r\n      gender,\r\n      age,\r\n      type: 'backend'\r\n\r\n    };\r\n\r\n    return Object.assign(\r\n      {},\r\n      state,\r\n      MakeBackendMagic(state),\r\n      DrinkSomeTea(state),\r\n      Procrastinate(state)\r\n    );\r\n  };\r\n\r\n  // FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube\r\n  const FrontendDeveloper = (name, gender, age) => {\r\n    let state = {\r\n      name,\r\n      gender,\r\n      age,\r\n      type: 'front'\r\n    };\r\n\r\n    return Object.assign(\r\n      {},\r\n      state,\r\n      MakeFrontendMagic(state),\r\n      DrinkSomeTea(state),\r\n      WatchYoutube(state)\r\n    );\r\n  };\r\n\r\n  // Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate\r\n  const Designer = (name, gender, age) => {\r\n    let state = {\r\n      name,\r\n      gender,\r\n      age,\r\n      type: 'design'\r\n    };\r\n\r\n    return Object.assign(\r\n      {},\r\n      state,\r\n      MakeItLooksBeautiful(state),\r\n      WatchYoutube(state),\r\n      Procrastinate(state)\r\n    );\r\n  };\r\n\r\n  //ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea\r\n  const ProjectManager = (name, gender, age) => {\r\n    let state = {\r\n      name,\r\n      gender,\r\n      age,\r\n      type: 'project'\r\n    };\r\n\r\n    return Object.assign(\r\n      {},\r\n      state,\r\n      DistributeTasks(state),\r\n      Procrastinate(state),\r\n      DrinkSomeTea(state)\r\n    );\r\n  };\r\n\r\n  const Nick = BackendDeveloper(\"Nick\", 'male', 25);\r\n        Nick.make();\r\n        Nick.drink();\r\n        Nick.procrastinate();\r\n        console.log(\"NICK\", Nick)\r\n\r\n  const Fred = FrontendDeveloper(\"Fred\", 'male', 33);\r\n        Fred.make();\r\n        Fred.drink();\r\n        Fred.watch();\r\n\r\n  const Anna = Designer(\"Anna\", 'female', 19);\r\n        Anna.make();\r\n        Anna.watch();\r\n        Anna.procrastinate();\r\n\r\n  const Max = ProjectManager(\"Max\", 'male', 22);\r\n        Max.distribute();\r\n        Max.procrastinate();\r\n        Max.drink();\r\n};\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (Composition);\r\n\n\n//# sourceURL=webpack:///./application/functions.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _functions__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./functions */ \"./application/functions.js\");\n\r\n/*\r\n  Composition:\r\n\r\n  Задание при помощи композиции создать объекты 4х типов:\r\n\r\n  functions:\r\n    - MakeBackendMagic\r\n    - MakeFrontendMagic\r\n    - MakeItLooksBeautiful\r\n    - DistributeTasks\r\n    - DrinkSomeTea\r\n    - WatchYoutube\r\n    - Procrastinate\r\n\r\n  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate\r\n  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube\r\n  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate\r\n  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea\r\n\r\n  ProjectManager(name,gender,age) => { name, gender, age, type: 'project'}\r\n\r\n*/\r\n\r\n\r\n\r\n\r\nObject(_functions__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ })

/******/ });
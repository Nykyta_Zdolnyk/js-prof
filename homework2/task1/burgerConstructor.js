/*

    Задание:

      1. Создать конструктор бургеров на прототипах, которая добавляет наш бургер в массив меню

      Дожно выглядеть так:
      new burger('Hamburger',[ ...Массив с ингредиентами ] , 20);

      Моделька для бургера:
      {
        cookingTime: 0,     // Время на готовку
        showComposition: function(){
          let {composition, name} = this;
          let compositionLength = composition.length;
          console.log(compositionLength);
          if( compositionLength !== 0){
            composition.map( function( item ){
                console.log( 'Состав бургера', name, item );
            })
          }
        }
      }

      Результатом конструктора нужно вывести массив меню c добавленными элементами.
      // menu: [ {name: "", composition: [], cookingTime:""},  {name: "", composition: [], cookingTime:""}]

        2. Создать конструктор заказов

        Моделька:
        {
          id: "",
          orderNumber: "",
          orderBurder: "",
          orderException: "",
          orderAvailability: ""
        }

          Заказ может быть 3х типов:
            1. В котором указано название бургера
              new Order('Hamburger'); -> Order 1. Бургер Hamburger, будет готов через 10 минут.
            2. В котором указано что должно быть в бургере, ищете в массиве Menu подходящий вариант
              new Order('', 'has', 'Название ингредиента') -> Order 2. Бургер Чизбургер, с сыром, будет готов через 5 минут.
            3. В котором указано чего не должно быть
              new Order('', 'except', 'Название ингредиента') ->


            Каждый их которых должен вернуть статус:
            "Заказ 1: Бургер ${Название}, будет готов через ${Время}

            Если бургера по условиям заказа не найдено предлагать случайный бургер из меню:
              new Order('', 'except', 'Булка') -> К сожалению, такого бургера у нас нет, можете попробовать "Чизбургер"
              Можно например спрашивать через Confirm подходит ли такой вариант, если да - оформлять заказ
              Если нет, предложить еще вариант из меню.

        3. Протестировать программу.
          1. Вначале добавляем наши бургеры в меню (3-4 шт);
          2. Проверяем работу прототипного наследования вызывая метод showComposition на обьект бургера с меню
          3. Формируем 3 заказа

        + Бонусные задания:
        4. Добавлять в исключения\пожелания можно несколько ингредиентов

  */

let id = 0;
const Ingredients = [
  "Булка",
  "Огурчик",
  "Котлетка",
  "Бекон",
  "Рыбная котлета",
  "Соус карри",
  "Кисло-сладкий соус",
  "Помидорка",
  "Маслины",
  "Острый перец",
  "Капуста",
  "Кунжут",
  "Сыр Чеддер",
  "Сыр Виолла",
  "Сыр Гауда",
  "Майонез"
];

var OurMenu = [];
var OurOrders = [];

function Burger(name, ingredients, cookingTime) {
  this.name = name;
  this.composition = ingredients;
  this.cookingTime = cookingTime;
  this.addBurgerToMenu();
}
Burger.prototype.addBurgerToMenu = function() {
  OurMenu.push(this);
};

Burger.prototype.showMenu = function() {
  console.log("Menu:", OurMenu);
};

Burger.prototype.showComposition = function() {
  let { composition, name } = this;
  let compositionLength = composition.length;
  console.log(compositionLength);
  if (compositionLength !== 0) {
    composition.map(function(item) {
      console.log("Состав бургера", name, item);
    });
  }
};

function Order(name, condition, value) {
  this.id = new Date().valueOf();
  this.orderNumber = ++id;
  this.orderBurger = name;
  this.orderException = condition;
  this.orderAvailability = value;

  this.checkTypeOfOrder();
}

Order.prototype.checkTypeOfOrder = function() {
  let { orderBurger } = this;

  if (orderBurger) {
    this.isNameOfBurger();
  } else {
    this.isCondition();
  }
};

Order.prototype.isNameOfBurger = function() {
  let { orderBurger, orderNumber } = this;

  let burger = OurMenu.filter(function(item) {
    return item.name == orderBurger;
  });

  if (burger.length) {
    burger.map(function(item) {
      console.log(
        `Order ${orderNumber}. Бургер ${item.name} будет готов через ${
          item.cookingTime
        } минут`
      );
    });
  } else console.log(`Бургера с названием '${orderBurger}' нету в меню. Проверьте название`)
  
};

Order.prototype.isCondition = function() {
  let { orderException, orderAvailability, orderNumber } = this;

  switch (orderException) {
    case "has":
      OurMenu.filter(function(burger) {
        return burger.composition.filter(function(item) {
          return item === orderAvailability;
        }).length;
      }).map(function(item) {
        console.log(
          `Order ${orderNumber}. Бургер ${
            item.name
          }, c ${orderAvailability}  будет готов через ${
            item.cookingTime
          } минут`
        );
      });
      break;

    case "except": // надо допилить
      console.log("except");

      let test = OurMenu.filter(function(burger, index) {
        return burger.composition.filter(function(item) {
          return item !== orderAvailability;
        }).length;
      })
      console.log(test)
      break;
    default:
      console.log('Заказ оформлен неверно!') 
    break;
  }
};

let hamburger = new Burger(
  "Hamburger",
  ["Булка", "Огурчик", "Котлетка", "Бекон"],
  10
);
let cheeseburger = new Burger(
  "Cheeseburger",
  ["Булка", "Рыбная котлета", "Соус карри", "Кисло-сладкий соус", "Помидорка"],
  15
);
let classic = new Burger(
  "Classic",
  ["Булка", "Маслины", "Острый перец", "Капуста", "Кунжут"],
  20
);
let blueCheese = new Burger(
  "Blue Cheese",
  ["Булка", "Сыр Чеддер", "Сыр Виолла", "Сыр Гауда", "Майонез"],
  25
);

// hamburger.addBurgerToMenu();
// cheeseburger.addBurgerToMenu();
// classic.addBurgerToMenu();
// blueCheese.addBurgerToMenu();
blueCheese.showMenu();

// let order = new Order("Classic");
// let order1 = new Order("", "has", "Сыр Виолла");
let order2 = new Order("", "except", "Кисло-сладкий соус");
// let order3 = new Order("", "", "Кисло-сладкий соус");
